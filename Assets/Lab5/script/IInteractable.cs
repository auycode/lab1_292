using UnityEngine;

namespace Phanuwit.GameDev3.Lab5.InteractionSystem
{
    public interface IInteractable
{
    void Interact(GameObject actor);
    }
}