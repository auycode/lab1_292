using UnityEngine;

namespace Phanuwit.GameDev3.Lab5.InteractionSystem
{
    public interface IActorEnterExitHandler {
     void ActorEnter(GameObject actor);
     void ActorExit(GameObject actor);
     }
}