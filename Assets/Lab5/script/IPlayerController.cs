using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Phanuwit.GameDev3.Lab5.PlayerController
{

    public interface IPlayerController
    {
        void MoveForward();
        void MoveForwardSprint();

        void MoveBackward();

        void TurnLeft();
        void TurnRight();
    }
}
