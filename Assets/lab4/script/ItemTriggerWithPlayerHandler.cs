using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Phanuwit.GameDev3.Lab4
{
    public class ItemTriggerWithPlayerHandler : MonoBehaviour
    {
        protected virtual void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                if (gameObject.CompareTag("Diamond"))
                {
                    ProcessTriggerWithDiamond();
                }
                else if (gameObject.CompareTag("Coin"))
                {
                    ProcessTriggerWithCoin();
                }

                var inventory = other.GetComponent<Inventory>();
                inventory.AddItem(gameObject.tag, 1);
                Destroy(gameObject);
            }
        }

        protected virtual void ProcessTriggerWithDiamond()
        {
            
        }

        protected virtual void ProcessTriggerWithCoin()
        {
            
        }
    }
}
