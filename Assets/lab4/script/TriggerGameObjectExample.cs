using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Phanuwit.GameDev3.Lab4
{
    public class TriggerGameObjectExample : MonoBehaviour
    {
        void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.tag == "Item")
            {
                Debug.Log("Do something with Item");
            }

            if (collider.gameObject.tag == "Player")
            {
                Debug.Log("Do something with Player");
            }
        }


        // Update is called once per frame
        void Update()
        {

        }
    }
}
