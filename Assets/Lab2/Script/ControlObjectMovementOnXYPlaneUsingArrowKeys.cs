﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Phanuwit.GameDev3.Chapter2.InputSystem

{
    public class ControlObjectMovementOnXYPlaneUsingArrowKeys : MonoBehaviour
    {
        public float m_MovementStep;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Keyboard keyboard = Keyboard.current;    
            
            if (keyboard[Key.LeftArrow].isPressed)
            {
                this.transform.Translate(-m_MovementStep, 0, 0);
            }
            if (keyboard[Key.RightArrow].isPressed)
            {
                this.transform.Translate(m_MovementStep, 0, 0);
            }
            if (keyboard[Key.UpArrow].isPressed)
            {
                this.transform.Translate(0, m_MovementStep, 0);
            }
            if (keyboard[Key.DownArrow].isPressed)
            {
                this.transform.Translate(0, -m_MovementStep, 0);
            }
        }
    }
}
