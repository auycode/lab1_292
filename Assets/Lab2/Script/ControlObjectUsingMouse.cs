using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Phanuwit.GameDev3.Chapter2

{
    public class ControlObjectUsingMouse : MonoBehaviour
    {
        private Vector3 m_MousePreviousPosition;
        public float m_MouseDeltaVectorScaling = 0.5f;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Vector3 mouseCurrentPos = Input.mousePosition;
            Vector3 mouseDeltaVector = Vector3.zero;
            mouseDeltaVector = (mouseCurrentPos - m_MousePreviousPosition).normalized;
            if (Input.GetMouseButtonUp(0))
            {
                this.transform.Translate(mouseDeltaVector * 
                                         m_MouseDeltaVectorScaling , Space.World);
            }
            else if (Input.GetMouseButtonDown(1))
            {
                this.transform.Translate(0, 0, Input.mouseScrollDelta.y *
                                               m_MouseDeltaVectorScaling , Space.World);
            }

            m_MousePreviousPosition = mouseCurrentPos;
        }
    }
}